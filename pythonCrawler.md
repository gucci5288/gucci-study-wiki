## python install

mac os 已經內建了pyhon, 所以不必安裝


# 查詢python版本
```bash
python3 --version
```

# 查詢Chrome版本

![chrome-version.png](chrome-version.png)

查詢結果

![chrome-version.png](chrome-version-result.png)

# 下載對應chromedriver

下載與Chrome同版本能使用的chromedriver

[chromedriver官方下載網址](https://chromedriver.chromium.org/downloads)


# 程式碼

main.py

```python
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from bs4 import BeautifulSoup  # Beautiful Soup 模組自動下載並解析網頁資料

options = Options()  # 啟動無頭模式
options.add_argument('--headless')  # 規避google bug
options.add_argument('--disable-gpu')

url1 = 'https://pala.tw/js-example/'

executable_path = '/path/to/chromedriver'  # 自行設定chromedriver放置的路徑

driver = webdriver.Chrome(executable_path=executable_path, options=options)
driver.get(url1)

pageSource = driver.page_source  # 取得網頁原始碼

soup = BeautifulSoup(pageSource, 'html.parser')

print('target url:', driver.current_url)
print('-----')
print(soup.prettify())
print('-----')
print(f'title : {soup.title}')

p_tags = soup.find_all('p')

for idx, tag in enumerate(p_tags):
    print(f'p-tags-{idx}:', tag)

driver.quit()  # 關閉瀏覽器


```

# 執行
```
phthon3 main.py
```
# 輸出
```
target url: https://pala.tw/js-example/
-----
<html lang="en">
 <head>
  <meta charset="utf-8"/>
  <title>
   消失的文字
  </title>
 </head>
 <body>
  <div id="word">
   JavaScript渲染才看得到這行
  </div>
  <p>
   其實上面還有段文字
  </p>
  <p>
   <script src="example.js" type="text/javascript">
   </script>
  </p>
 </body>
</html>
-----
title : <title>消失的文字</title>
p-tags-0: <p>其實上面還有段文字</p>
p-tags-1: <p>
<script src="example.js" type="text/javascript"></script>
</p>
```
